define('SC.Horizon.ApplicationSkeleton.Layout', [
    'ApplicationSkeleton.Layout',
    'SC.Configuration',
    'underscore'
], function ThemeExtensionApplicationSkeletonLayout(
    ApplicationSkeletonLayout,
    Configuration,
    _
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            // NOTE: Using this approach since the native module DOES NOT have a defined getContext function
            // The addExtraContextProperty helper function DID NOT work.
            ApplicationSkeletonLayout.prototype.getContext = _.wrap(ApplicationSkeletonLayout.prototype.getContext, function getContext(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));

                return {
                    // @property {Boolean} fixedHeader
                    fixedHeader: Configuration.get('header.fixedHeader')
                };
            });
        }
    };
});
