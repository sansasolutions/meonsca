/*
    © 2017 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module Footer
define('SC.Horizon.Footer.Simplified', [
    'Footer.Simplified.View',
    'underscore',
    'SC.Configuration'
],
function ThemeExtensionFooterSimplified(
    FooterView,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            // for Copyright message
            var initialConfigYear = Configuration.get('footer.copyright.initialYear');
            var initialYear = initialConfigYear ? parseInt(initialConfigYear, 10) : new Date().getFullYear();
            var currentYear = new Date().getFullYear();

            FooterView.addExtraContextProperty(
                'extraFooterSimplifiedView',
                'object',
                function FooterViewAddExtraContextProperty() {
                    return {
                        // @property {String} backgroundUrl
                        backgroundUrl: Configuration.get('footer.backgroundImg'),
                        copyright: {
                            hide: !!Configuration.get('footer.copyright.hide'),
                            companyName: Configuration.get('footer.copyright.companyName'),
                            initialYear: initialYear,
                            currentYear: currentYear,
                            showRange: initialYear < currentYear
                        },
                        text: Configuration.get('footer.text')
                    };
                }
            );
        }
    };
});
