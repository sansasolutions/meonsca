define('SC.Horizon.ItemRelations.SC.Configuration', [
    'SC.Configuration'
],
function ThemeExtensionItemRelations() {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            if (SC.CONFIGURATION.bxSliderDefaults && SC.CONFIGURATION.bxSliderDefaults.slideWidth) {
                SC.CONFIGURATION.bxSliderDefaults.slideWidth = 533;
                SC.CONFIGURATION.bxSliderDefaults.maxSlides = 3;
            }
        }
    };
});
