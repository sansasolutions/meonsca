define('SC.Horizon.GlobalViews.Breadcrumb.View', [
    'GlobalViews.Breadcrumb.View'
],
function ThemeExtensionBreadcrumbView(
    BreadcrumbView
) {
    'use strict';

    return {

        mountToApp: function mountToApp() {
            BreadcrumbView.addExtraContextProperty(
                'extraBreadcrumbView',
                'object',
                function BreadcrumbViewAddExtraContextProperty(context) {
                    var pages = context.pages;
                    var pageBanner;
                    var showCategHeading = false;
                    if (pages.length > 0) {
                        pageBanner = pages[pages.length - 1].pageBanner;
                        showCategHeading = (pages[pages.length - 1].isCategory) && (pages[pages.length - 1].pageBanner !== '');
                    }

                    return {
                        pageBanner: pageBanner,
                        showCategHeading: showCategHeading
                    };
                }
            );
        }
    };
});
