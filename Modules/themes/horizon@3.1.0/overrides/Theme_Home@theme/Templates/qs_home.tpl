{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{!-- Edited for Horizon Theme --}}

<div class="home">
    <!-- HERO PROMO -->
    {{#if extraHomeView.hero}}
    <div class="home-hero">
        <div class="hero-wrapper" style="background-image: url('{{getThemeAssetsPath extraHomeView.hero.image}}');"></div>
        <div class="home-info-container">
            <a href="{{extraHomeView.hero.href}}">
                <div class="home-info">
                    {{#if extraHomeView.hero.title}}
                        <h2 class="home-info-title">{{extraHomeView.hero.title}}</h2>
                    {{/if}}
                    {{#if extraHomeView.hero.text}}
                        <h3 class="home-info-text">{{extraHomeView.hero.text}}</h3>
                    {{/if}}
                    <span class="home-info-linktext">
                        {{#if extraHomeView.hero.linktext}}
                            {{extraHomeView.hero.linktext}}
                        {{else}}
                            {{translate 'Shop Now'}}
                        {{/if}}
                    </span>
                </div>
            </a>
        </div>
    </div>
    {{/if}}

    <!-- ANNOUNCEMENT -->
    {{#if extraHomeView.announcement}}
    <div class="home-announcement">{{translate extraHomeView.announcement}}</div>
    {{/if}}

    <!-- CMS MERCHANDISING ZONE -->
    <div class="home-merchandizing-zone">
        <div class="home-merchandizing-zone-content">
            <div data-cms-area="home_merchandizing_zone" data-cms-area-filters="path"></div>
        </div>
    </div>

    <!--
    INFOBLOCKS
    The first two infloblocks are styled to span 50% of the viewport on larger devices
    Any subsequent infoblocks span full width
    -->
    <div class="home-infoblock-layout">
        {{#each extraHomeView.infoBlocks}}
        <a{{objectToAtrributes item}} class="home-infoblock-link">
            <div class="home-infoblock {{#unless singleInfoblock}}home-infoblock{{@index}}{{/unless}}"
            style="background-image: url('{{getThemeAssetsPath image}}'); background-color:{{#if color}}{{color}}{{else}}darkgray{{/if}};">
                <div class="home-infoblock-content">
                    <div class="home-infoblock-info">
                        {{#if title}}
                            <h2 class="home-info-title">{{title}}</h2>
                        {{/if}}
                        {{#if text}}
                            <h3 class="home-info-text">{{text}}</h3>
                        {{/if}}
                        <span class="home-info-linktext">
                            {{#if linktext}}
                                {{linktext}}
                            {{else}}
                                {{translate 'Shop Now'}}
                            {{/if}}
                        </span>
                    </div>
                </div>
            </div>
        </a>
        {{/each}}
    </div>

    <!-- CMS ZONE -->
    <div class="home-cms-zone" data-cms-area="home_content_middle" data-cms-area-filters="path"></div>

    <!-- CAROUSEL -->
    {{#if extraHomeView.showCarousel}}
    <div class="home-slider-container">
        <div class="home-image-slider">
            <ul data-slider class="home-image-slider-list">
                {{#each extraHomeView.carousel}}
                <li>
                    <div class="home-slide-main-container">
                        <img src="{{getThemeAssetsPath image}}" class="home-slide-image">
                        <div class="home-info-container">
                            <a{{objectToAtrributes item}}>
                                <div class="home-info">
                                    {{#if title}}<h2 class="home-info-title">{{title}}</h2>{{/if}}
                                    {{#if text}}<h3 class="home-info-text">{{text}}</h3>{{/if}}
                                    <span class="home-info-linktext">{{#if linktext}}{{linktext}}{{else}}{{translate 'Shop Now'}}{{/if}}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                {{/each}}
            </ul>
        </div>
    </div>
    {{/if}}

    <!-- CMS ZONE -->
    <div class="home-cms-zone" data-cms-area="home_content_bottom" data-cms-area-filters="path"></div>

</div>

{{!----
Use the following context variables when customizing this template:

	imageHomeSize (String)
	imageHomeSizeBottom (String)
	carouselImages (Array)
	bottomBannerImages (Array)

----}}
