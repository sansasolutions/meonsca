/*
    © 2017 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module Home
define('SC.Horizon.Home', [
    'Home.View',
    'PluginContainer',
    'underscore',
    'SC.Configuration'
], function ThemeExtensionHome(
    HomeView,
    PluginContainer,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            // for Carousel
            var carousel = Configuration.get('home.horizonCarouselImages', []);
            var infoBlocks = Configuration.get('home.infoblock', []);
            var heros = Configuration.get('home.hero', []);
            var singleInfoblock = (infoBlocks.length === '1');
            var firstHero = heros[0];
            var carouselObj;
            var showCarousel = false;

            HomeView.addExtraContextProperty(
                'extraHomeView',
                'object',
                function HomeViewAddExtraContextProperty(context) {
                    carouselObj = context.carousel;
                    showCarousel = context.showCarousel;

                    if (!_.isEmpty(carouselObj)) {
                        _.each(carouselObj, function mapGroupBanner(val, key) {
                            if (_.isEmpty(val.image)) {
                                val.image = carousel[key].image || '';
                            }
                        });
                    } else {
                        if (typeof showCarousel === 'undefined') {
                            showCarousel = true;
                        }

                        carouselObj = carousel;
                    }


                    return {
                        // @property {Boolean} showCarousel
                        showCarousel: showCarousel && carouselObj && !!carouselObj.length,
                        // @property {Array<Object>} carousel
                        carousel: carouselObj,
                        // @property {Boolean} singleInfoblock
                        singleInfoblock: singleInfoblock,
                        // @property {Array<Object>} infoblock
                        infoBlocks: infoBlocks,
                        // @property {String} announcement
                        announcement: _(Configuration.get('home.announcement')).translate(),
                        // @property {Array<Object>} hero
                        hero: firstHero
                    };
                }
            );
        }
    };
});
