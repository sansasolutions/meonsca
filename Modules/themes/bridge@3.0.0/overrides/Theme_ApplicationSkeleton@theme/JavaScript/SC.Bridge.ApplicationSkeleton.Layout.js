define('SC.Bridge.ApplicationSkeleton.Layout', [
    'ApplicationSkeleton.Layout',
    'SC.Configuration',
    'underscore'
], function SCApplicationSkeletonLayout(
    ApplicationSkeletonLayout,
    Configuration,
    _
) {
    'use strict';

    ApplicationSkeletonLayout.prototype.getContext = _.wrap(ApplicationSkeletonLayout.prototype.getContext, function getContext(fn) {
        fn.apply(this, _.toArray(arguments).slice(1));
        return {
            // @property {Boolean} fixedHeader
            fixedHeader: Configuration.get('header.fixedHeader')
        };
    });
});
