/*
    © 2017 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module Home
define('SC.Bridge.Home', [
    'Home.View',
    'PluginContainer',
    'underscore',
    'SC.Configuration'
], function SCHome(
    HomeView,
    PluginContainer,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            var carousel = Configuration.get('home.carouselImages', []);
            var topPromo = Configuration.get('home.topPromo', []);
            var infoblock = Configuration.get('home.infoblock', []);
            var freeTextImages = Configuration.get('home.freeTextImages', []);
            var infoblockTile = false;
            var infoblockFive = false;
            var showCarousel = false;
            var carouselObj;

            var $el;
            var isHomeView;

            application.getLayout().on('afterAppendView', function applicationGetLayout() {
                $el = application.getLayout().$el;
                if (application.getLayout().currentView instanceof HomeView === true) {
                    isHomeView = $el.hasClass('home-view');

                    if (!isHomeView) {
                        $el.addClass('home-view');
                    }
                } else {
                    isHomeView = $el.hasClass('home-view');

                    if (isHomeView) {
                        $el.removeClass('home-view');
                    }
                }
            });

            HomeView.addExtraContextProperty(
                'extraHomeViewContext',
                'object',
                function HomeAddExtraContextProperty(context) {
                    carouselObj = context.carousel;
                    showCarousel = context.showCarousel;

                    if (!_.isEmpty(carouselObj)) {
                        _.each(carouselObj, function mapGroupBanner(val, key) {
                            if (_.isEmpty(val.image)) {
                                val.image = carousel[key].image || '';
                            }
                        });
                    } else {
                        if (typeof showCarousel === 'undefined') {
                            showCarousel = true;
                        }

                        carouselObj = carousel;
                    }

                    if (infoblock.length === 3 || infoblock.length > 5) {
                        infoblockTile = true;
                    }

                    if (infoblock.length === 5) {
                        infoblockFive = true;
                    }

                    return {
                        // @property {Boolean} showCarousel
                        showCarousel: showCarousel && carouselObj && !!carouselObj.length,
                        // @property {Array<Object>} carousel
                        carousel: carouselObj,
                        // @property {String} carouselBgrImg
                        carouselBgrImg: Configuration.get('home.carouselBgrImg'),
                        // @property {Number} infoblockCount
                        infoblockCount: infoblock.length,
                        // @property {Boolean} infoblockTile
                        infoblockTile: infoblockTile,
                        // @property {Boolean} infoblockFive
                        infoblockFive: infoblockFive,
                        // @property {Array<Object>} freeTextImages
                        infoblock: infoblock,
                        // @property {String} freeTextTitle - allows markup
                        freeText: _(Configuration.get('home.freeText', '')).translate(),
                        // @property {String} freeTextTitle
                        freeTextTitle: _(Configuration.get('home.freeTextTitle')).translate(),
                        // @property {Boolean} showFreeTextImages
                        showFreeTextImages: freeTextImages && !!freeTextImages.length,
                        // @property {Array<Object>} freeTextImages - the object contains the properties text:String, href:String
                        freeTextImages: freeTextImages,
                        // @property {Array<Object>} topPromo
                        topPromo: topPromo
                    };
                }
            );
        }
    };
});
