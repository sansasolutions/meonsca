{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{! Edited for Bridge Theme }}

<div class="error-management-page-not-found">
    <div class="error-management-page-not-found-header" style="{{#if ErrorManagementViewContext.backgroundImage}}background-image: url({{getThemeAssetsPath ErrorManagementViewContext.backgroundImage}});{{/if}}{{#if ErrorManagementViewContext.backgroundColor}}background-color: {{ErrorManagementViewContext.backgroundColor}};{{/if}}">
		<div class="error-management-page-not-found-caption" style="{{#if ErrorManagementViewContext.captionBackgroundImg}}background-image: url({{getThemeAssetsPath ErrorManagementViewContext.captionBackgroundImg}});{{/if}}">
			<div class="error-management-page-not-found-caption-content">
				<div class="error-management-page-not-found-title">
					{{#if ErrorManagementViewContext.title}}
					<h1>{{{ErrorManagementViewContext.title}}}</h1>
					{{else}}
					<h1>{{ErrorManagementViewContext.pageHeader}}</h1>
					{{/if}}
					{{#if ErrorManagementViewContext.text}}
					<p class="error-management-page-not-found-text">{{ErrorManagementViewContext.text}}</p>
					{{/if}}
				</div>
				{{#if ErrorManagementViewContext.btnText}}
				<div class="error-management-page-not-found-button-container">
					<a href="{{ErrorManagementViewContext.btnHref}}" class="error-management-page-not-found-button">
						{{ErrorManagementViewContext.btnText}}
					</a>
				</div>
				{{/if}}
			</div>
		</div>
    </div>

	<div id="error-management-page-not-found-content" class="error-management-page-not-found-content"></div>

	<div id="error-management-page-not-found-cms" class="error-management-page-not-found-cms" data-cms-area="page_not_found_cms" data-cms-area-filters="page_type"></div>
</div>
