define('ItemBadges.Model', [
    'Backbone.CachedModel',
    'underscore',
    'Utils'
], function ItemBadgesModel(
    CachedModel,
    _,
    Utils
) {
    'use strict';

    return CachedModel.extend({
        urlRoot: Utils.getAbsoluteUrl('services/ItemBadges.Service.ss')
    });
});
