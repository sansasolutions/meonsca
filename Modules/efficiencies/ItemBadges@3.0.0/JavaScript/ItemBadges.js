define('ItemBadges', [
    'SC.Configuration',
    'ItemBadges.Helper',
    'ItemBadges.Collection',

    'ItemBadges.View',
    'ProductDetails.Full.View',
    'ProductDetails.QuickView.View',
    'Facets.ItemCell.View',
    'ItemRelations.RelatedItem.View',

    'underscore'
], function ItemBadges(
    Configuration,
    Helper,
    Collection,

    ItemBadgesView,
    ProductDetailsFullView,
    ProductDetailsQuickViewView,
    FacetsItemCellView,
    ItemRelationsRelatedItemView,

    _
) {
    'use strict';

    var itemBadgesCollection;
    return {
        mountToApp: function mountToApp(application) {
            var pdp = application.getComponent('PDP');
            var moduleConfig = Configuration.get('itemBadges');

            if (moduleConfig.showInFacet) {
                Configuration.facets.push({
                    id: 'custitem_ef_badges',
                    priority: 20,
                    behavior: 'multi',
                    name: _(moduleConfig.facetName).translate()
                });
            }

            pdp.on('afterShowContent', function afterShowContent() {
                if (!itemBadgesCollection) {
                    itemBadgesCollection = new Collection();
                }

                itemBadgesCollection.fetch();
            });

            pdp.on('beforeShowContent', function beforeShowContent() {
                itemBadgesCollection = new Collection();
            });

            pdp.addChildViews(
                'ProductDetails.Full.View', {
                    'Product.ImageGallery': {
                        'Itembadges.View': {
                            childViewIndex: 5,
                            childViewConstructor: function childViewConstructor() {
                                return new ItemBadgesView({
                                    application: application,
                                    pdpItemInfo: pdp.getItemInfo(),
                                    collection: itemBadgesCollection
                                });
                            }
                        }
                    }
                }
            );

            pdp.addChildViews(
                'ProductDetails.QuickView.View', {
                    'Product.ImageGallery': {
                        'Itembadges.View': {
                            childViewIndex: 5,
                            childViewConstructor: function childViewConstructor() {
                                return new ItemBadgesView({
                                    application: application,
                                    pdpItemInfo: pdp.getItemInfo(),
                                    collection: itemBadgesCollection
                                });
                            }
                        }
                    }
                }
            );

            Helper.addChildView(FacetsItemCellView, {
                target: '.facets-item-cell-grid-link-image,' +
                '.facets-item-cell-list-image, .facets-item-cell-table-link-image',
                viewClass: 'itembadges-listitem'
            });

            Helper.addChildView(ItemRelationsRelatedItemView, {
                target: '.item-relations-related-item-thumbnail',
                viewClass: 'itembadges-relateditem'
            });
        }
    };
});
