define('ItemBadges.Helper', [
    'PluginContainer',
    'ItemBadges.Collection',

    'ItemBadges.View',
    'underscore'
], function ItemBadgesHelper(
    PluginContainer,
    Collection,

    ItemBadgesView,
    _
) {
    'use strict';

    var Helper = {
        addChildView: function addChildView(View, options) {
            var collection = new Collection();

            _.extend(View.prototype, {
                initialize: _.wrap(View.prototype.initialize, function wrapInitialize(fn) {
                    fn.apply(this, _.toArray(arguments).slice(1));
                    this.listenTo(this, 'afterViewRender', function afterViewRender() {
                        this.$(options.target).after('<div class="itemBadge-view-container ' +
                            options.viewClass + '" data-view="Itembadges.View"></div>');
                        this.renderChild('Itembadges.View');
                    });

                    collection.fetch();
                }),

                childViews: _.extend(View.prototype.childViews || {}, {
                    'Itembadges.View': function wrapperFunction() {
                        return new ItemBadgesView({
                            application: this.application,
                            model: this.model,
                            collection: collection
                        });
                    }
                })
            });
        }
    };

    return Helper;
});
