define('ItemBadges.Collection', [
    'Backbone',
    'Backbone.CachedCollection',
    'ItemBadges.Model',
    'underscore',
    'Utils'
], function ItemBadgesCollection(
    Backbone,
    BackboneCachedCollection,
    Model,
    _,
    Utils
) {
    'use strict';

    return BackboneCachedCollection.extend({
        model: Model,

        url: Utils.getAbsoluteUrl('services/ItemBadges.Service.ss'),

        filterBadges: function filterBadges(badges) {
            var itemBadges = [];
            var data;

            if (badges) {
                _.each(badges.split(','), function each(value) {
                    itemBadges.push(value.trim());
                });

                data = _.filter(this.models, function filter(badge) {
                    return _.contains(itemBadges, badge.get('name').trim());
                });
            }

            return new Backbone.Collection(data);
        }
    });
});
