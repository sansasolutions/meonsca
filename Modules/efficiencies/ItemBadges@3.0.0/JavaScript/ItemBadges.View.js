define('ItemBadges.View', [
    'Backbone',
    'Backbone.CollectionView',

    'ItemBadges.List.View',
    'itembadges_view.tpl'
], function ItemBadgesView(
    Backbone,
    CollectionView,

    ItemBadgesList,
    Template
) {
    'use strict';

    return Backbone.View.extend({
        template: Template,

        initialize: function initialize(options) {
            this.application = options.application;
            this.pdpItemInfo = options.pdpItemInfo;
            this.collection = options.collection;

            this.listenToOnce(this.collection, 'sync', this.render);
        },

        getContext: function getContext() {
            var item;
            var badge = false;

            if (this.pdpItemInfo) {
                item = this.pdpItemInfo.item || this.pdpItemInfo;
                badge = item.custitem_ef_badges;
            } else {
                item = this.model.get('item') || this.model;
                if (item.get('custitem_ef_badges') !== '&nbsp;') {
                    badge = item.get('custitem_ef_badges');
                }
            }

            this.badgeCollection = this.collection.filterBadges(badge || false);

            return {
                hasBadges: !!this.badgeCollection
            };
        },

        childViews: {
            'Itembadges.List.View': function ItembadgesLisView() {
                return new CollectionView({
                    collection: this.badgeCollection,
                    childView: ItemBadgesList
                });
            }
        }
    });
});
