define('BackInStockNotification.PlugToViewHelper', [
    'SC.Configuration'
], function BackInStockNotificationPlugToViewHelper(
    Configuration
) {
    'use strict';

    // This is a helper to plug the BIS button without edditing the template. It's not needed, if you want to put
    // the control in other place by editing the template, you can do it.
    // Or you can code the inject on view function of your preference.
    // The example appends it AFTER the product list control
    return {

        injectOnView: function injectOnView(view, configuration) {
            if (configuration.injectOnViewFunction) {
                configuration.injectOnViewFunction(view);
            } else {
                view.$('.product-details-full-actions')
                    .after('<section class="back-in-stock-notification-placeholder" data-type="backinstocknotification-control-placeholder" />');
            }
        },

        mountToApp: function mountToApp(application, configuration) {
            var pdp = application.getComponent('PDP');
            var enablementConfiguration = Configuration.get('quickStart.backinstocknotification');

            var self = this;
            if (enablementConfiguration.enabled) {
                pdp.on('afterShowContent', function afterShowContent() {
                    var view = application.getLayout().getCurrentView();
                    if (view && view.model && view.model.getSelectedMatrixChilds) {
                        if (configuration.injectOnViewMode === 'code') {
                            self.injectOnView(view, configuration);
                        }
                    }
                });
            }

            // pdp.addChildViews(
            //     'ProductDetails.Full.View', {
            //         'SocialSharing.Flyout': {
            //             'BackInStock.View': {
            //                 childViewIndex: 5,
            //                 childViewConstructor: function childViewConstructor() {
            //                     return new DisplayProductLeadTimeView({

            //                     });
            //                 }
            //             }
            //         }
            //     }
            // );
        }
    };
});
