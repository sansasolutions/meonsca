# Landing Page Templates
This module is used to store a number of Site Management Tools (SMT), also known as CMS, landing page templates.

You will need to log into the CMS, create a new landing page, and insert/drag an HTML container. Once the HTML container is placed, copy and paste the template markup. Make edits as needed.

## TEMPLATES

1) About Us - qs-smt-about-us-tpl.html
2) Frequently Asked Questions (FAQ) - qs-smt-faq-tpl.html
3) Privacy Policy - qs-smt-privacy-policy-tpl.html
4) Shop By Brand - qs-smt-shop-by-brand-tpl.html
5) Shop By Category - qs-smt-shop-by-category-tpl.html

## PLACEHOLDER IMAGES

1) about-us-staff-placeholder.png
2) brands-logo-placeholder.png

### NOTES:
- Placeholder images are available in the Images directory. (LandingPages@dev/Images)
- Placeholder images will need to be uploaded manually to the file cabinet, anywhere within the Web Hosting Files folder. A recommendation is to create a cms folder within the assets folder and upload any necessary files there. (Ex. Web Hosting Files > Live Hosting Files > assets > cms) Current templates have that path as the image source.
- Update image source if needed.

## CSS - STYLES

Each theme has a scss file to has all the SMT template styles, which are theme specific.
    - Modules > themes > theme_name > CustomStyles@dev > Sass > _qs-custom-smt-pages.scss

## DOCS

https://confluence.corp.netsuite.com/x/MuoJB