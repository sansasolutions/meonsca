define('SizeChart', [
    'SizeChart.Router',
    'ItemSizeChartLink.View',
    'SC.CCT.SizeChart.LinkView',
    'ProductDetails.Full.View',
    'SizeChart.Model',
    'underscore'
], function SizeChart(
    Router,
    ItemSizeChartLink,
    CCTSizeChartLinkView,
    ProductDetailsFullView,
    Model
) {
    'use strict';

    var sizeChartModel;
    var internalid;

    return {
        mountToApp: function mountToApp(application) {
            var pdp = application.getComponent('PDP');

            application.getComponent('CMS').registerCustomContentType({
                id: 'sizechart',
                view: CCTSizeChartLinkView
            });

            pdp.on('afterShowContent', function afterShowContent() {
                internalid = pdp.getItemInfo().item.custitem_ef_sc_size_chart_id;
                if (internalid) {
                    sizeChartModel.fetch({
                        data: {
                            internalid: internalid
                        }
                    });
                }
            });

            pdp.on('beforeShowContent', function beforeShowContent() {
                sizeChartModel = new Model();
            });

            pdp.addChildViews(
                'ProductDetails.Full.View', {
                    'Product.Sku': {
                        'Item.SizeChart': {
                            childViewIndex: 4,
                            childViewConstructor: function childViewConstructor() {
                                return new ItemSizeChartLink({
                                    pdpItemInfo: pdp.getItemInfo(),
                                    sizeChartModel: sizeChartModel
                                });
                            }
                        }
                    }
                });

            return new Router(application);
        }
    };
});
