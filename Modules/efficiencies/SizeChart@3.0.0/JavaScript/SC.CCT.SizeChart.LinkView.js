define('SC.CCT.SizeChart.LinkView', [
    'CustomContentType.Base.View',
    'item_size_chart_link.tpl',
    'underscore'
], function SCCCTSizeChartLinkView(
    CustomContentTypeBaseView,
    itemSizeChartLinkTpl,
    _
) {
    return CustomContentTypeBaseView.extend({
        template: itemSizeChartLinkTpl,

        getContext: function getContext() {
            var sizeChartSettings = this.settings;

            return {
                internalid: sizeChartSettings.custrecord_cct_sc_sizechart,
                linkText: _.translate(sizeChartSettings.custrecord_cct_sc_text_link || 'Size Chart')
            };
        }
    });
});
