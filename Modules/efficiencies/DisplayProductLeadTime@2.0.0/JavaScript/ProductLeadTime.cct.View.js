define('ProductLeadTime.cct.View', [
    'CustomContentType.Base.View',
    'DisplayProductLeadTime.Model',
    'display_product_lead_time.tpl',
    'underscore'
], function SCACCTItemNameView(
    CustomContentTypeBaseView,
    DisplayProductLeadTimeModel,
    Template,
    _
) {
    return CustomContentTypeBaseView.extend({
        template: Template,

        contextDataRequest: ['item'],

        initialize: function initalize() {
            this.model = new DisplayProductLeadTimeModel();

            this.listenToOnce(this.model, 'sync', this.render);
        },

        getContext: function getContext() {
            var settings = this.settings;
            var item = this.contextData.item();
            var message = settings.custrecord_cct_dplt_title || 'Expected back in stock';
            var showLeadTime = false;

            this.model.fetch({
                data: {
                    itemId: item.get('internalid')
                }
            });

            if (this.model.get('purchaseOrder') && this.model.get('leadTime') && !item.get('isinstock')) {
                showLeadTime = true;
            }

            return {
                msgOutOfStock: _.translate(message),
                outOfStockDate: this.model.getLeadTime(),
                showLeadTime: showLeadTime
            };
        }
    });
});
