define('DisplayProductLeadTime', [
    'ProductDetails.Full.View',
    'DisplayProductLeadTime.Model',
    'DisplayProductLeadTime.View',
    'ProductLeadTime.cct.View'
], function DisplayProductLeadTime(
    ProductDetailsFullView,
    DisplayProductLeadTimeModel,
    DisplayProductLeadTimeView,
    ProductLeadTimeCctView
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            application.getComponent('CMS').registerCustomContentType({
                id: 'displayproductleadtime',
                view: ProductLeadTimeCctView
            });
        }
    };
});
