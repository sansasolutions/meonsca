define('WishlistRename.ProductList.Edit.View', [
    'WishlistRename.Helper',

    'ProductList.Edit.View',
    'ProductList.Item.Collection',
    'MenuTree.View',

    'underscore'
], function WishlistRenameProductListEditView(
    Helper,

    ProductListEditView,
    ProductListItemCollection,
    MenuTreeView,

    _
) {
    'use strict';


    _.extend(ProductListEditView.prototype, {
        onSaveComplete: function onSaveComplete() {
            /* eslint-disable */
            var self = this;
            var url = Helper.generateUrl();

            if (_.isArray(self.model.get('items')))
            {
                self.model.set('items', new ProductListItemCollection(self.model.get('items')));
            }

            self.$containerModal && self.$containerModal.modal('hide');

            if (self.isEdit)
            {
                self.application.ProductListModule.Utils.getProductLists().add(self.model, {merge: true});
                MenuTreeView.getInstance().updateMenuItemsUI();
                self.parentView.render();

                if (self.parentView.$el.hasClass('ProductListDetailsView'))
                {
                    self.parentView.showConfirmationMessage(
                        _('Good! The list was successfully updated. ').translate()
                    );
                }
                else
                {
                    self.parentView.showConfirmationMessage(
                        _('Good! Your <a href="/"+ url +"/$(0)">$(1)</a> list was successfully updated. ').translate(self.model.get('internalid'), self.model.get('name'))
                    );
                }
            }
            else
            {
                self.application.ProductListModule.Utils.getProductLists().add(self.model);
                MenuTreeView.getInstance().updateMenuItemsUI();
                self.parentView.render();
                self.parentView.showConfirmationMessage(
                    _('Good! Your <a href="/"+ url +"/$(0)">$(1)</a> list was successfully created. ').translate(self.model.get('internalid'), self.model.get('name'))
                );
            }
            self.parentView.highlightList && self.parentView.highlightList(self.model.get('internalid'));
            /* eslint-enable */
        }
    });
});
