define('WishlistRename.ProductList.ControlSingle.View', [
    'WishlistRename.Helper',
    'wishlistrename_product_list_control_single.tpl',

    'ProductList.ControlSingle.View',

    'underscore'
], function WishlistRenameProductListControlSingleView(
    Helper,
    Template,

    ProductListControlSingleView,

    _
) {
    'use strict';

    _.extend(ProductListControlSingleView.prototype, {
        template: Template,

        getContext: _.wrap(ProductListControlSingleView.prototype.getContext, function ProductListControlSingleViewGetContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                renameAdded: _('Added to ' + Helper.getName()).translate(),
                renameNotAdded: _('Add to ' + Helper.getName()).translate()
            });

            return context;
        })
    });
});
