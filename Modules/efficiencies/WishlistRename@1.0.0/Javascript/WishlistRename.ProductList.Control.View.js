define('WishlistRename.ProductList.Control.View', [
    'WishlistRename.Helper',
    'wishlistrename_product_list_control.tpl',

    'ProductList.Control.View',
    'MenuTree.View',

    'underscore'
], function WishlistRenameProductListControlView(
    Helper,
    Template,

    ProductListControlView,
    MenuTreeView,

    _
) {
    'use strict';

    _.extend(ProductListControlView.prototype, {
        template: Template,

        moveProduct: function moveProduct(destination) {
            /* eslint-disable */
            var self = this
                ,	original_item = this.moveOptions.productListItem
                ,	original_item_clone = original_item.clone()
                ,	details_view = this.moveOptions.parentView;

            original_item_clone.set('productList', {
                id: destination.get('internalid')
            });

            destination.get('items').create(original_item_clone,
                {
                    validate: false
                    ,	success: function (saved_model)
                {
                    var app = details_view.application
                        ,	from_list = self.application.ProductListModule.Utils.getProductLists().findWhere({internalid: self.moveOptions.parentView.model.get('internalid') })
                        ,	to_list = self.application.ProductListModule.Utils.getProductLists().findWhere({internalid: destination.get('internalid')});
                    var url = Helper.generateUrl();

                    self.doMoveProduct(from_list, to_list, original_item, saved_model);

                    details_view.model.get('items').remove(original_item);
                    details_view.render();

                    jQuery('.sc-flyout-bg').remove();

                    MenuTreeView.getInstance().updateMenuItemsUI();
                    app.getLayout().currentView.showConfirmationMessage(
                        _('<div class="product-list-control-message">Good! You successfully moved the item from this to <a href="/"+ url +"/$(0)">$(1)</a></div>').
                        translate(destination.get('internalid'), destination.get('name'))
                    );
                }
            });
            /* eslint-enable */
        },

        getContext: _.wrap(ProductListControlView.prototype.getContext, function ProductListControlViewGetContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                productLists: this.product_list_collection || [],
                rename: _('Add to ' + Helper.getName()).translate()
            });

            return context;
        })
    });
});
