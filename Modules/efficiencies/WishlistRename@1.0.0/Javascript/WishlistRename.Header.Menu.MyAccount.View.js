define('WishlistRename.Header.Menu.MyAccount.View', [
    'WishlistRename.Helper',

    'Header.Menu.MyAccount.View',

    'wishlistrename_header_menu_myaccount.tpl',

    'underscore'
], function WishlistRenameHeaderMenuMyAccountView(
    Helper,

    HeaderMenuMyAccountView,

    Template,

    _
) {
    'use strict';

    _.extend(HeaderMenuMyAccountView.prototype, {
        template: Template,

        getContext: _.wrap(HeaderMenuMyAccountView.prototype.getContext, function HeaderMenuMyAccountViewGetContext(fn) {
            var context = fn.call(this);
            var urlConfig = Helper.generateUrl();

            if (this.product_list_collection) {
                this.product_list_collection.each(function eachProductListCollection(productList) {
                    var url = urlConfig + '/' + (productList.get('internalid') || 'tmpl_' + productList.get('templateId'));
                    productList.set('url', url, { silent: true });
                });
            }

            _.extend(context, {
                productLists: this.product_list_collection || [],
                rename: _(Helper.getName()).translate(),
                renameUrl: urlConfig
            });

            return context;
        })
    });
});
