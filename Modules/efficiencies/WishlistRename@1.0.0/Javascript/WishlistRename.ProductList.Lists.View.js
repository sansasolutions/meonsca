define('WishlistRename.ProductList.Lists.View', [
    'Backbone',

    'WishlistRename.Helper',
    'wishlistrename_product_list_lists.tpl',

    'ProductList.Lists.View',
    'LiveOrder.Model',

    'underscore'
], function WishlistRenameProductListListsView(
    Backbone,

    Helper,
    Template,

    ProductListListsView,
    LiveOrderModel,

    _
) {
    'use strict';

    var url = Helper.generateUrl();
    var name = Helper.getName();

    _.extend(ProductListListsView.prototype, {
        title: _(name).translate(),

        template: Template,

        addListToCart: function addListToCart(list) {
            /* eslint-disable */
            // collect the items data to add to cart
            var lines_to_add = []
                ,	self = this
                ,	not_purchasable_items_count = 0;

            list.get('items').each(function (pli)
            {
                if (pli.get('item').get('_isPurchasable'))
                {
                    lines_to_add.push(pli);
                }
                else
                {
                    not_purchasable_items_count++;
                }
            });

            if (lines_to_add.length === 0)
            {
                var errorMessage = _('All items in the list are not available for purchase.').translate();

                self.showWarningMessage(errorMessage);

                return;
            }

            // add the items to the cart and when its done show the confirmation view
            LiveOrderModel.getInstance().addProducts(lines_to_add).done(function ()
            {
                // before showing the confirmation view we need to fetch the items of the list with all the data.
                self.application.ProductListModule.Utils.getProductList(list.get('internalid')).done(function(model)
                {
                    self.addedToCartView = new ProductListAddedToCartView({
                        application: self.application
                        ,	parentView: self
                        ,	list: new ProductListModel(model) //pass the model with all the data
                        ,	not_purchasable_items_count: not_purchasable_items_count
                    });

                    // also show a confirmation message
                    var confirmMessage;

                    if (list.get('items').length > 1)
                    {
                        confirmMessage =  _('Good! $(0) items from your <a class="product-list-name" href="/"+ url +"/$(1)">$(2)</a> list were successfully added to your cart. You can continue to <a href="">view your cart and checkout</a>')
                            .translate(list.get('items').length, list.get('internalid'), list.get('name'));
                    }
                    else
                    {
                        confirmMessage =  _('Good! $(0) item from your <a class="product-list-name" href="/"+ url +"/$(1)">$(2)</a> list was successfully added to your cart. You can continue to <a href="" data-touchpoint="viewcart">view your cart and checkout</a>')
                            .translate(list.get('items').length, list.get('internalid'), list.get('name'));
                    }

                    self.showConfirmationMessage(confirmMessage);
                    self.application.getLayout().showInModal(self.addedToCartView);
                });
            });
            /* eslint-enable */
        },

        getBreadcrumbPages: function getBreadcrumbPages() {
            return {
                text: _(name).translate(),
                href: '/' + url
            };
        },

        navigateToItems: function navigateToItems(e) {
            /* eslint-disable */
            // ignore clicks on anchors and buttons
            if (_.isTargetActionable(e))
            {
                return;
            }

            var list = this.getCurrentList(e)
                ,	internalid = list.get('internalid')
                ,	url = '/'+ url +'/' + (internalid ? internalid : 'tmpl_' + list.get('templateid'));

            Backbone.history.navigate(url, {trigger: true});
            /* eslint-enable */
        },

        getContext: _.wrap(ProductListListsView.prototype.getContext, function ProductListListsViewGetContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                rename: _(Helper.getName()).translate()
            });

            return context;
        })
    });
});
