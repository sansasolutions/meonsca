define('WishlistRename.ProductList.Details.View', [
    'WishlistRename.Helper',
    'wishlistrename_product_list_details.tpl',

    'ProductList.Details.View',

    'underscore'
], function WishlistRenameProductListDetailsView(
    Helper,
    Template,

    ProductListDetailsView,

    _
) {
    'use strict';

    var url = Helper.generateUrl();

    _.extend(ProductListDetailsView.prototype, {
        template: Template,

        getBreadcrumbPages: function getBreadcrumbPages() {
            /* eslint-disable */
            var breadcrumb = [
                {
                    text: _(Helper.getName()).translate(),
                    href: '/' + url
                }
                ,	{
                    text: this.model.get('name'),
                    href: '/'+ url +'/' + (this.model.get('internalid') ? this.model.get('internalid') : 'tmpl_' + this.model.get('templateid'))
                }
            ];
            if (this.application.ProductListModule.Utils.isSingleList())
            {
                breadcrumb.splice(0, 1); //remove first
            }
            return breadcrumb;
            /* eslint-enable */
        },

        getContext: _.wrap(ProductListDetailsView.prototype.getContext, function ProductListDetailsViewGetContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                url: url,
                rename: _('Go to ' + Helper.getName()).translate()
            });

            return context;
        })
    });
});
