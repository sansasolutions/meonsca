define('WishlistRename.Helper', [
    'SC.Configuration'
], function WishlistRenameHelper(
    Configuration
) {
    'use strict';

    var Helper = {
        configuration: Configuration && Configuration.get('wishlistRename'),

        getName: function getName() {
            return Helper.configuration.newName;
        },

        generateUrl: function generateUrl() {
            var rawName = Helper.configuration.newName;

            return rawName.replace(/ /g, '').toLowerCase();
        }
    };

    return Helper;
});
