define('WishlistRename', [
    'WishlistRename.Header.Menu.MyAccount.View',
    'WishlistRename.ProductList.Control.View',
    'WishlistRename.ProductList.ControlSingle.View',
    'WishlistRename.ProductList.Details.View',
    'WishlistRename.ProductList.Edit.View',
    'WishlistRename.ProductList.ListDetails.View',
    'WishlistRename.ProductList.Lists.View',
    'WishlistRename.ProductList.Router'
], function WishlistRename() {
    'use strict';
});
