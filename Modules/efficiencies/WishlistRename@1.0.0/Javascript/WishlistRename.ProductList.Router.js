define('WishlistRename.ProductList.Router', [
    'Backbone',

    'WishlistRename.Helper',

    'ProductList.Router',

    'underscore'
], function WishlistRenameProductListRouter(
    Backbone,
    Helper,

    ProductListRouter,

    _
) {
    'use strict';

    _.extend(ProductListRouter.prototype, {
        routes: {},

        initialize: _.wrap(ProductListRouter.prototype.initialize, function ProductListRouterInitialize(fn, application) {
            var url = Helper.generateUrl();

            this.route(url, 'showProductListsList');
            this.route(url + '/?*options', 'showProductListsList');
            this.route(url + '/:id', 'showProductListDetails');
            this.route(url + '/:id/?*options', 'showProductListDetails');

            this.application = application;
        })
    });
});
