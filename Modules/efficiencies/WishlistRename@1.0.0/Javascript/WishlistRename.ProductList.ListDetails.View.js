define('WishlistRename.ProductList.ListDetails.View', [
    'WishlistRename.Helper',
    'wishlistrename_product_list_list_details.tpl',

    'ProductList.ListDetails.View',

    'underscore'
], function WishlistRenameProductListListDetailsView(
    Helper,
    Template,

    ProductListListDetailsView,

    _
) {
    'use strict';

    _.extend(ProductListListDetailsView.prototype, {
        template: Template,

        getContext: _.wrap(ProductListListDetailsView.prototype.getContext, function ProductListControlSingleView(fn) {
            var context = fn.call(this);
            var url = Helper.generateUrl();

            _.extend(context, {
                renameUrl: url
            });

            return context;
        })
    });
});
