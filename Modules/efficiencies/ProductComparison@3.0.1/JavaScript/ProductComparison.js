// module ProductComparison
define('ProductComparison', [
    'Backbone',
    'SC.Configuration',

    'ProductComparison.Router',

    'Facets.Browse.View',
    'Facets.Browse.View.Extended',
    'Facets.ItemCell.View.Extended',

    'underscore',
    'Utils'

], function ProductComparison(
    Backbone,
    Configuration,

    Router,

    FacetsBrowseView
) {
    'use strict';

    var ProductComparisonConfig = Configuration.get('productComparison');

    if (ProductComparisonConfig && ProductComparisonConfig.enabled) {
        return {
            mountToApp: function mountToApp(application) {
                var layout = application.getLayout();

                application.getLayout().on('afterAppendView', function applicationGetLayout(view) {
                    if (!(view instanceof FacetsBrowseView)) {
                        view.$el.find('.facets-item-cell-addtocompare').remove();
                    }
                });


                layout.goToProductComparison = function goToProductComparison() {
                    Backbone.history.navigate('productcomparison', { trigger: true });
                };

                return new Router(application);
            }
        };
    }
});
