define('CustomerSegments.Home.View', [
    'Backbone',
    'SC.Configuration',

    'CustomerSegments.Helper',

    'Home.View',

    'Utils',

    'jQuery',
    'underscore'
], function CustomerSegmentsBanner(
    Backbone,
    Configuration,

    Helper,

    HomeView,

    Utils,

    jQuery,
    _
) {
    'use strict';

    _.extend(HomeView.prototype, {
        configuration: Configuration && Configuration.get('customerSegments'),

        initialize: _.wrap(HomeView.prototype.initialize, function wrapInitialize(fn) {
            var self = this;
            var carouselObj = {};
            var carouselImages = [];
            var configBanners = _.map(Configuration.get('home.carouselImages', []), function mapCarouselImages(url) {
                if (_.isObject(url)) {
                    return {
                        href: url.href,
                        image: url.image.indexOf(_.getAbsoluteUrl()) === -1 ? _.getAbsoluteUrl(url.image) : url.image,
                        linktext: url.linktext,
                        text: url.text,
                        title: url.title
                    };
                }

                return Utils.getAbsoluteUrlOfNonManagedResources(url);
            });

            this.model = new Backbone.Model({
                showCarousel: false,
                carousel: [],
                carouselImages: []
            });

            jQuery.when(Helper.setGroupsInfo()).done(function whenSetGroupsInfo(response) {
                if (response.groupBanners) {
                    carouselObj = Helper.setBannerImage(response.groupBanners, configBanners);

                    _.each(carouselObj, function eachCarouselObj(val) {
                        carouselImages.push(val.image);
                    });

                    self.model.set('carousel', carouselObj);
                    self.model.set('carouselImages', carouselImages);
                } else {
                    self.model.set('carousel', configBanners);
                    self.model.set('carouselImages', configBanners);
                }

                self.model.set('showCarousel', true);
            });

            fn.apply(this, _.toArray(arguments).slice(1));

            this.listenTo(this.model, 'all', jQuery.proxy(this.render, this));
        }),

        getContext: _.wrap(HomeView.prototype.getContext, function getContext(fn) {
            var context = fn.call(this);

            _.extend(context, {
                carouselImages: this.model.get('carouselImages'),
                carousel: this.model.get('carousel'),
                showCarousel: this.model.get('showCarousel')
            });

            return context;
        })
    });
});

