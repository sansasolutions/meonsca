define('CustomerSegments.ProductViews.Price.View', [
    'Backbone',
    'ProductViews.Price.View',
    'ProductList.DisplayFull.View',
    'ProductList.Details.View',
    'ListHeader.View',
    'Transaction.Line.Views.Price.View',

    'CustomerSegments.Helper',

    'OrderHistory.Item.Actions.View',
    'ReorderItems.Actions.AddToCart.View',

    'ProductDetails.Full.View',
    'ProductDetails.QuickView.View',

    'SC.Configuration',

    'jQuery',
    'underscore',
    'Utils'
], function CustomerSegmentsProductViewsPriceView(
    Backbone,
    ProductViewsPriceView,
    ProductListDisplayFullView,
    ProductListDetailsView,
    ListHeaderView,
    TransactionLineViewsPriceView,

    Helper,

    OrderHistoryItemActionsView,
    ReorderItemsActionsAddToCartView,

    ProductDetailsFullView,
    ProductDetailsQuickViewView,

    Configuration,

    jQuery,
    _
) {
    'use strict';

    var itemHepler = {
        isHidePrices: false,
        isHideAddtoCart: false,

        hidePrice: function hidePrice() {
            var self = this;

            this.productListDisplayFullView();
            this.productListDetailsView();
            this.transactionLinePriceView();

            ProductViewsPriceView.prototype.getContext =
                _.wrap(ProductViewsPriceView.prototype.getContext, function ProductViewsPriceViewFunction(fn) {
                    var context = fn.call(this);
                    var itemGroups = self.getItemGroups(context.model);
                    var result;

                    if (itemGroups && self.isHiden(itemGroups, 'isHidePrices')) {
                        this.on('beforeCompositeViewRender', function afterViewRender() {
                            this.$el.find('.product-views-price-login-to-see-prices').html('');
                        });

                        result = {
                            line: '',
                            isPriceRange: '',
                            showComparePrice: false,
                            currencyCode: '',
                            priceFormatted: '',
                            price: '',
                            comparePrice: '',
                            minPrice: '',
                            maxPrice: '',
                            comparePriceFormatted: '',
                            minPriceFormatted: '',
                            maxPriceFormatted: ''
                        };

                        _.extend(context, result);
                    }

                    return context;
                });
        },

        productListDisplayFullView: function productListDisplayFullView() {
            var self = this;
            ProductListDisplayFullView.prototype.initialize = _.wrap(ProductListDisplayFullView.prototype.initialize, function initialize(fn) {
                var itemGroups = self.getItemGroups(this.model);
                fn.apply(this, _.toArray(arguments).slice(1));

                if (itemGroups && (self.isHiden(itemGroups, 'isHideAddtoCart')
                    || self.isHiden(itemGroups, 'isHidePrices'))) {
                    this.on('afterViewRender', function afterViewRender() {
                        this.$el.find('.product-list-display-full-select').html('');
                    });
                }
            });
        },

        productListDetailsView: function productListDetailsView() {
            var self = this;
            ProductListDetailsView.prototype.getContext =
                _.wrap(ProductListDetailsView.prototype.getContext, function ProductViewsPriceViewFunction(fn) {
                    var context = fn.call(this);
                    var itemGroups;
                    var isItemHasNoPrice = false;

                    _.each(this.model.get('items').models, function eachItemModel(item) {
                        itemGroups = self.getItemGroups(item.get('item'));
                        if (itemGroups && self.isHiden(itemGroups, 'isHidePrices')) {
                            isItemHasNoPrice = true;
                        }
                    });


                    if (isItemHasNoPrice) {
                        ListHeaderView.prototype.getContext =
                            _.wrap(ListHeaderView.prototype.getContext, function ListHeaderViewFunction(ListHeaderViewfn) {
                                var ListHeaderViewContext = ListHeaderViewfn.call(this);

                                _.extend(ListHeaderViewContext, {
                                    showSelectAll: false
                                });

                                return ListHeaderViewContext;
                            });
                    }

                    return context;
                });
        },

        transactionLinePriceView: function transactionLinePriceView() {
            var self = this;
            TransactionLineViewsPriceView.prototype.getContext =
                _.wrap(TransactionLineViewsPriceView.prototype.getContext, function CartItemFunction(fn) {
                    var context = fn.call(this);
                    var itemGroups = self.getItemGroups(this.model);
                    var result;

                    if (itemGroups && self.isHiden(itemGroups, 'isHidePrices')) {
                        result = {
                            price: '',
                            rateFormatted: '',
                            showComparePrice: '',
                            comparePriceFormatted: ''
                        };

                        _.extend(context, result);
                    }

                    return context;
                });
        },

        hideAddToCart: function hideAddToCart() {
            this.hideCartBtn(ProductDetailsFullView);
            this.hideCartBtn(ProductDetailsQuickViewView);
            this.hideCartBtn(OrderHistoryItemActionsView);
            this.hideCartBtn(ReorderItemsActionsAddToCartView);
        },

        hideCartBtn: function hideCartBtn(View) {
            var self = this;
            var itemGroups;

            View.prototype.initialize = _.wrap(View.prototype.initialize, function initialize(fn) {
                fn.apply(this, _.toArray(arguments).slice(1));
                itemGroups = self.getItemGroups(this.model);

                if (itemGroups && (self.isHiden(itemGroups, 'isHideAddtoCart')
                    || self.isHiden(itemGroups, 'isHidePrices'))) {
                    this.on('afterViewRender', function afterViewRender() {
                        this.$el.find('[data-view="MainActionView"]').remove();
                        this.$el.find('.order-history-item-actions-reorder').remove();
                        this.$el.find('.reorder-items-actions-add-to-cart-button-container').remove();
                    });
                }
            });
        },

        isHiden: function isHiden(itemGroups, type) {
            var hiddenGroups = false;
            if (type === 'isHidePrices') {
                hiddenGroups = _.filter(this.isHidePrices, function fnIsHidePrices(cg) {
                    return itemGroups.includes(cg.groups) && cg.hidePrice;
                });
            } else if (type === 'isHideAddtoCart') {
                hiddenGroups = _.filter(this.isHideAddtoCart, function fnIsHideAddtoCart(cg) {
                    return itemGroups.includes(cg.groups) && cg.hideAddToCart;
                });
            }

            return (hiddenGroups && !_.isEmpty(hiddenGroups));
        },

        getItemGroups: function getItemGroups(itemModel) {
            var itemGroups = false;

            if (itemModel.get('custitem_item_customersegments')) {
                itemGroups = itemModel.get('custitem_item_customersegments').split(',');
            } else if (itemModel.get('item')) {
                itemGroups = itemModel.get('item').get('custitem_item_customersegments');
                if (!itemGroups || _.isEmpty(itemGroups)) {
                    itemGroups = false;
                } else {
                    itemGroups = itemGroups.split(',');
                }
            }

            return itemGroups;
        }
    };
    return itemHepler;
});
