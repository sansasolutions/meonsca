define('CustomerSegments.Checkout', [
    'CustomerSegments.Helper',

    'CustomerSegments.Header.Logo.View',
    'CustomerSegments.Checkout.Step',
    'CustomerSegments.Transaction.Line.Views.Cell.Navigable.View',
    'Utils'
], function CustomerSegments(
    Helper
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            application.getLayout().on('beforeAppendView', function applicationGetLayout() {
                // Check if Facets is already been add to searchApiMasterOptions
                if (!Helper.inFacets('custitem_item_customersegments')) {
                    // Update searchApiMasterOptions to add Customer Segment Facets
                    Helper.addToSearchApiMasterOptions();
                }
            });
        }
    };
});
