define('CustomerSegments.ProductLine.Stock.View', [
    'ProductLine.Stock.View',
    'customersegments_product_line_stock.tpl',
    'underscore'
], function CustomerSegmentsItemViewsStockView(
    ItemViewsStock,
    Template,
    _
) {
    'use strict';

    _.extend(ItemViewsStock.prototype, {
        template: Template
    });

    ItemViewsStock.addExtraContextProperty(
        'showView',
        'boolean',
        function addExtraContextProperty(context) {
            return !!_.isUndefined(context.view);
        }
    );
});
