define('CustomerSegments.Myaccount', [
    'CustomerSegments.Helper',

    'Item.Collection',

    'CustomerSegments.OrderHistory.Packages.View',
    'CustomerSegments.ProductLine.Stock.View',
    'CustomerSegments.Header.Logo.View',
    'CustomerSegments.Header.View'
], function CustomerSegmentsMyaccount(
    Helper,

    ItemCollection
) {
    'use strict';

    return {
        mountToApp: function mountToApp(application) {
            Helper.filterCollection(ItemCollection);

            application.getLayout().on('beforeAppendView', function applicationGetLayout() {
                // Check if Facets is already been add to searchApiMasterOptions
                if (!Helper.inFacets('custitem_item_customersegments')) {
                    // Update searchApiMasterOptions to add Customer Segment Facets
                    Helper.addToSearchApiMasterOptions();
                }
            });
        }
    };
});
