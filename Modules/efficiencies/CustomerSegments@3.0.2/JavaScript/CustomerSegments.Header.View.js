define('CustomerSegments.Header.View', [
    'SC.Configuration',
    'CustomerSegments.Helper',
    'Profile.Model',
    'Header.View',
    'jQuery',
    'underscore'
], function CustomerSegmentsHeaderView(
    Configuration,
    Helper,
    ProfileModel,
    HeaderView,
    jQuery,
    _
) {
    'use strict';

    _.extend(HeaderView.prototype, {
        initialize: _.wrap(HeaderView.prototype.initialize, function getContext(fn) {
            var categories = _.clone(Configuration.navigationData);
            var self = this;

            fn.apply(this, _.toArray(arguments).slice(1));

            Configuration.navigationData = [];
            jQuery.when(Helper.setGroupsInfo()).done(function whenSetGroupsInfo(response) {
                if (response.groupCategory) {
                    categories = _.filter(categories, function filterCategories(cat) {
                        return _.indexOf(response.groupCategory, cat.text) !== -1 || !cat.categories;
                    });
                }

                Configuration.navigationData = categories;

                self.render();
            });
        })
    });
});
