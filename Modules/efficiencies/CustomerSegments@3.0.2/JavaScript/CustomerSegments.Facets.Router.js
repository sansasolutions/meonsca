define('CustomerSegments.Facets.Router', [
    'Backbone',

    'CustomerSegments.Helper',

    'Facets.Browse.View',
    'Facets.Router',
    'Facets.Model',
    'Facets.Helper',
    'Categories.Model',
    'AjaxRequestsKiller',
    'Profile.Model',

    'jQuery',
    'underscore'
], function CustomerSegmentsFacetsRouter(
    Backbone,

    Helper,

    FacetsBrowseView,
    FacetsRouter,
    FacetsModel,
    FHelper,
    CategoriesModel,
    AjaxRequestsKiller,
    ProfileModel,

    jQuery,
    _
) {
    'use strict';

    _.extend(FacetsRouter.prototype, {
        facetLoading: function facetLoading() {
            // Check if Facets is already been add to searchApiMasterOptions
            if (!Helper.inFacets('custitem_item_customersegments')) {
                // Update searchApiMasterOptions to add Customer Segment Facets
                Helper.addToSearchApiMasterOptions(this);
            } else {
                this.showPage();
            }
        },
        categoryLoading: function categoryLoading() {
            // Check if Facets is already been add to searchApiMasterOptions
            if (!Helper.inFacets('custitem_item_customersegments')) {
                // Update searchApiMasterOptions to add Customer Segment Facets
                Helper.addToSearchApiMasterOptions(this, true);
            } else {
                this.showPage(true);
            }
        },
        showPage: function showPage(isCategoryPage) {
            /* eslint-disable */
            var self = this
                ,	facetModel = new FacetsModel()
                ,	fullurl = Backbone.history.fragment
                ,	models = [facetModel]
                ,	translator = FHelper.parseUrl(fullurl, this.translatorConfig, isCategoryPage);

            facetModel.options = {
                data: translator.getApiParams()
                ,	killerId: AjaxRequestsKiller.getKillerId()
                ,	pageGeneratorPreload: true
            };

            //if prices aren't to be shown we take out price related facet
            //and clean up the url
            if (ProfileModel.getInstance().hidePrices())
            {
                translator = translator.cloneWithoutFacetId('onlinecustomerprice');
                Backbone.history.navigate(translator.getUrl());
            }

            if (isCategoryPage)
            {
                var categoryModel = new CategoriesModel();

                categoryModel.options = {
                    data: { 'fullurl': translator.getCategoryUrl() }
                    ,	killerId: AjaxRequestsKiller.getKillerId()
                };

                facetModel.set('category', categoryModel);
                models.push(categoryModel);
            }

            jQuery.when.apply(null, _.invoke(models, 'fetch', {}))
                .then(function (facetResponse)
                {
                    facetResponse = isCategoryPage ? facetResponse[0] : facetResponse;

                    if (facetResponse.corrections && facetResponse.corrections.length > 0)
                    {
                        var unaliased_url = self.unaliasUrl(fullurl, facetResponse.corrections);

                        if (SC.ENVIRONMENT.jsEnvironment === 'server')
                        {
                            nsglobal.statusCode = 301;
                            nsglobal.location = '/' + unaliased_url;
                        }
                        else
                        {
                            Backbone.history.navigate('#' + unaliased_url, {trigger: true});
                        }
                    }
                    else
                    {
                        var view = new FacetsBrowseView({
                            translator: translator
                            ,	translatorConfig: self.translatorConfig
                            ,	application: self.application
                            ,	model: facetModel
                        });

                        if (facetModel.get('category')) {
                            jQuery.when(Helper.setGroupsInfo()).done(function whenSetCustomerGroups(response) {
                                var categories = _.map(response.groupCategory, function mapCategories(n) {
                                    return n.toLowerCase();
                                });

                                var category = [facetModel.get('category').get('breadcrumb')[0].name.toLowerCase()];

                                if (_.isEmpty(_.intersection(categories, category)) && !_.isEmpty(response.customerGroups)) {
                                    self.application.getLayout().notFound();
                                } else {
                                    translator.setLabelsFromFacets(facetModel.get('facets') || []);
                                    view.showContent();
                                }
                            });
                        } else {
                            translator.setLabelsFromFacets(facetModel.get('facets') || []);
                            view.showContent();
                        }
                    }
                });
            /* eslint-enable */
        }
    });
});
