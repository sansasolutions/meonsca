define('InventoryDisplay.ItemViews.Stock.View', [
    'Backbone',
    'inventory_display_stock_views.tpl'
], function InventoryDisplayItemViewsStockView(
    Backbone,
    inventoryDisplayStockViewsTpl
) {
    'use strict';

    return Backbone.View.extend({
        template: inventoryDisplayStockViewsTpl,
        initialize: function initialize() {
            this.model.on('change', this.render, this);
            this.pdpItemInfo = this.options.pdpItemInfo;
        },
        getContext: function getContext() {
            var newMOdel;
            var originalModel = this.model;
            var childs = originalModel.getSelectedMatrixChilds();
            var stockInfo;

            if (childs && childs.length === 1) {
                originalModel = childs[0];
                newMOdel = originalModel;
            } else {
                newMOdel = originalModel.get('item');
            }

            stockInfo = newMOdel.getStockInfo();

            return {
                showOutOfStockMessage: !!(!stockInfo.isInStock && stockInfo.showOutOfStockMessage),
                stockInfo: stockInfo,
                showInStockMessage: newMOdel.get('_showInStockMessageForPDP', true),
                inStockMessage: newMOdel.get('_inStockMessageForPDP'),
                stockAvailable: newMOdel.get('quantityavailable'),
                showStockAvailable: newMOdel.get('quantityavailable') && (newMOdel.get('quantityavailable') !== 0),
                isNotAvailableInStore: stockInfo.isNotAvailableInStore
            };
        }
    });
});
