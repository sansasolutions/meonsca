/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

define('EF.HandlebarsExtras', [
    'SC.Configuration',
    'Utils',
    'GlobalViews.Message.View',
    'Handlebars',
    'Backbone',
    'underscore'
], function HandlebarsExtras(
    Configuration,
    Utils,
    GlobalViewsMessageView,
    Handlebars,
    Backbone,
    _
) {
    'use strict';

    // {{ternary value "si" "no"}}
    Handlebars.registerHelper('ternary', function ternaryHelper() {
        // Last argument is the options object.
        var params = arguments;

        return (params[0] === true) ? new Handlebars.SafeString(params[1]) : new Handlebars.SafeString(params[2]);
    });

    // {{debug}} or {{debug someValue}}
    Handlebars.registerHelper('debug', function debugHelper(optionalValue) {
        console.log('\nCurrent Context');  // eslint-disable-line no-console
        console.log('===================='); // eslint-disable-line no-console
        console.log(this); // eslint-disable-line no-console

        if (arguments.length > 1) {
            console.log('Value'); // eslint-disable-line no-console
            console.log('===================='); // eslint-disable-line no-console
            console.log(optionalValue); // eslint-disable-line no-console
        }
    });

    // {{ternary value "si" "no"}}
    Handlebars.registerHelper('getAbsoluteURL', function getAbsoluteURLHelper() {
        // Last argument is the options object.
        return new Handlebars.SafeString(_.getAbsoluteUrl(arguments[0]));
    });
});
