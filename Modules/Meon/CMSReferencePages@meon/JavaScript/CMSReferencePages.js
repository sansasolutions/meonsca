define('CMSReferencePages', function(){

    'use strict';

    return {
        displayVideo: function(e)
        {
            e.preventDefault();

            var $target = jQuery(e.currentTarget),
                video_url = $target.attr('data-video-url'),
                container = $target.closest('[data-video-container]'),
                allow ="autoplay; encrypted-media";


            if(video_url) {

                if(container.find('iframe') && container.find('iframe').length){
                    container.find('iframe').remove();
                }

                var iframe = jQuery('<iframe />').attr({
                    src: video_url,
                    allow: allow,
                    webkitallowfullscreen: true,
                    mozallowfullscreen: true,
                    allowfullscreen: true
                });

                container.append(iframe);

                iframe.on('load', function(){
                    iframe.addClass('visible');
                });

            }
        },

        mountToApp: function (Application)
        {
            var layout = Application.getLayout();

            _.extend(layout, {
                displayVideo: this.displayVideo
            });

            _.extend(layout.events, {
                'click [data-action="show-video"]': 'displayVideo'
            });
        }
    }
});
