{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div class="contact-us-container">
    <header class="contact-us-header">
        <!-- header -->
				<div class="banner left">
	        <img src="/c.4026778_SB1/sca-dev-kilimanjaro/img/Contactus/sample_banner.jpg" class="background">

	        <div class="container">
	            <h1 class="green">Contact</h1>
	            <h1>Our <strong>Helpful</strong> Team</h1>
	        </div><!-- container -->
    		</div>
				<!-- header -->
        <div id="contact-us-header-cms" class="contact-us-header-cms" data-cms-area="contact-us-header-cms" data-cms-area-filters="path">
				</div>
    </header>

    <div class="contact-us-content ">
			<div class="page_content wide">
        <div class="container">
            <div class="contact col-xs-12 col-lg-6">
                <h3>Our Details</h3>
                <div class="column  col-xl-6">
                    <p><strong>Call us on</strong></p>
                    <h5>0808 118 1922</h5>
                </div><!-- column -->
                <div class="column col-xl-6">
                    <p class="special">Opening Hours (7.00 - 16.30 Monday to Friday)</p>
                </div><!-- column -->
                <div class="line col-xs-12">

								</div>
                <div class="column col-xs-12">
                    <p><strong>Email us</strong></p>
                    <h5><a href="mailto:mail@meonuk.com">mail@meonuk.com</a></h5>
                </div><!-- column -->
								<div class="line col-xs-12">

								</div>
                <div class="column col-xs-12 col-xl-6">
                    <p><strong>UK (Head Office)</strong></p>
                    <p>Meon LTD,<br>Railside,<br>Northarbour Spur,<br>PORTSMOUTH,<br>PO6 3TU<br>T: 023 9220 0606</p>
                </div><!-- column -->
                <div class="column col-xs-12 col-xl-6">
                    <p><strong>Ireland</strong></p>
                    <p>Carrick House,<br>48 Bridge Road,<br>Burren,<br>WARRENPOINT,<br>BT34 3QT
                        <br>NI: 028 3085 0049
                        <br>ROI: 01 840 7647</p>
                </div><!-- column -->
            </div><!-- contact -->
            <div class="contact right col-xs-12 col-lg-6">
                <h3>Leave a Message</h3>
								<div id="contact-us-form-cms" class="contact-us-form-cms" data-cms-area="contact-us-form-cms" data-cms-area-filters="path"></div>
				        <div class="contact-us-alert-placeholder" data-type="alert-placeholder"></div>
				        <small class="contact-us-required">
				            {{translate 'Required'}}<span class="contact-us-form-required">*</span>
				        </small>

				        <form action="#" class="contact-us-form" novalidate>
				            {{#each additionalFields}}
				                <div class="contact-us-form-controls-group" data-validation="control-group">
				                    <label class="contact-us-form-label" for="{{fieldId}}">
				                        {{translate '$(0)' displayName}}
				                        {{#if mandatory}}
				                            {{translate '<small class="contact-us-form-required">*</small>'}}
				                        {{/if}}
				                    </label>
				                    <div class="contact-us-form-controls" data-validation="control">
				                        <input  data-action="text" type="text" name="{{fieldId}}" id="{{fieldId}}" class="contact-us-form-input" value="" maxlength="300"></input>
				                    </div>
				                </div>
				            {{/each}}

				            <div class="contact-us-form-controls-group" data-validation="control-group">
				                <label class="contact-us-form-label" for="email">
				                    {{translate 'Email <small class="contact-us-form-required">*</small>'}}
				                </label>
				                <div class="contact-us-form-controls" data-validation="control">
				                    <input  data-action="text" type="text" name="email" id="email" class="contact-us-form-input" value="" maxlength="300"></input>
				                    <p>{{ translate '(If you have an account with us,<br /> please enter the email address associated with your account.)' }}</p>
				                </div>
				            </div>


				            <div class="contact-us-form-controls-group" data-validation="control-group">
				                <label class="contact-us-form-label" for="title">
				                    {{translate 'Title <small class="contact-us-form-required">*</small>'}}
				                </label>
				                <div class="contact-us-form-controls" data-validation="control">
				                    <input  data-action="text" type="text" name="title" id="title" class="contact-us-form-input" value="" maxlength="300"></input>
				                </div>
				            </div>

				            <div class="contact-us-form-controls-group" data-validation="control-group">
				                <label  class="contact-us-form-label" for="incomingmessage">
				                    {{translate 'Comments <small class="contact-us-form-required">*</small>'}}
				                </label>
				                <div class="contact-us-form-controls" data-validation="control">
				                    <textarea name="incomingmessage" id="incomingmessage" class="contact-us-form-textarea"></textarea>
				                </div>
				            </div>

				            <div id="contact-us-extras-cms" class="contact-us-extras-cms" data-cms-area="contact-us-extras-cms" data-cms-area-filters="path"></div>

				            <div class="hide message"></div>

				            <div class="contact-us-form-controls-group" data-action="show-modal" data-toggle="show-in-modal">
				                <button type="submit" class="newsletter-subscription-form-button-subscribe">{{translate 'Submit'}}</button>
				            </div>
				        </form>
            </div><!-- contact -->
        </div><!-- container -->
    </div>
		</div>

</div>
