{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{! Edited for Summit Theme }}

<div class="home-nopad">
	<!-- CAROUSEL -->
	{{#if extraHome.showCarousel}}
	<!-- Extended Carousel to include slide text and links, overrides standard carousel -->
	<div class="home-slider-container">
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				{{#each extraHome.carousel}}
					<li>
						<div class="home-slide-main-container home-slide{{@index}}"{{#if image}} style="background-image: url({{getThemeAssetsPathWithDefault image}})"{{/if}}>
								<div class="filter-dark"></div>
							<img src="{{getThemeAssetsPathWithDefault image 'img/summit-carousel-home-1.jpg'}}" class="home-slide-image" style="display: none;" />
							<div class="home-slide-caption">
								{{#if title}}<h2 class="home-slide-caption-title">{{title}}</h2>{{/if}}
								{{#if text}}<p>{{text}}</p>{{/if}}
								<div class="home-slide-caption-button-container">
									<a{{objectToAtrributes item}} class="home-slide-caption-button">{{#if text}}{{linktext}}{{else}}{{translate 'Shop now'}}{{/if}} <i class="home-slide-button-icon"></i></a>
								</div>
							</div>
						</div>
					</li>
				{{/each}}
			</ul>
		</div>
	</div>
	{{else}}
	<!-- Standard Carousel -->
	<div class="home-slider-container">
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				{{#each carouselImages}}
					<li>
						<div class="home-slide-main-container" style="background-image: url({{this}});">
							<img src="{{this}}" class="home-slide-image" style="display: none;" />
							<div class="home-slide-caption">
								<h2 class="home-slide-caption-title">SAMPLE HEADLINE</h2>
								<p>Example descriptive text displayed on multiple lines.</p>
								<div class="home-slide-caption-button-container">
									<a href="/search" class="home-slide-caption-button">Shop Now <i class="home-slide-button-icon"></i></a>
								</div>
							</div>
						</div>
					</li>
				{{/each}}
			</ul>
		</div>
	</div>
	{{/if}}

</div>

<div class="home-nopad">
	<div class="home-cms-zone" data-cms-area="home_content_top" data-cms-area-filters="path"></div>

	<!-- CMS MERCHANDISING ZONE -->
	<div class="home-merchandizing-zone">
		<div class="home-merchandizing-zone-content">
			<div data-cms-area="home_merchandizing_zone" data-cms-area-filters="path"></div>
		</div>
	</div>

	<!--
    INFOBLOCKS
    Use the Configuration section under Layout > Infoblocks
	Two infoblocks per row
	-->
	{{#if extraHome.showInfoblocks}}
    <div class="home-infoblock-layout">
        {{#each extraHome.infoblock}}
		<a href="{{href}}" class="home-infoblock-link">
			<div class="home-infoblock home-infoblock{{@index}}" style="{{#if image}}background-image: url({{getThemeAssetsPathWithDefault image}});{{/if}}{{#if color}}background-color: {{color}};{{/if}}">
				<div class="home-infoblock-content">
					{{#if title}}
		            <h2 class="home-infoblock-title">{{title}}</h2>
					{{/if}}
					{{#if text}}
		            <h3 class="home-infoblock-text">{{text}}</h3>
					{{/if}}
		        </div>
			</div>
		</a>
		{{/each}}
	</div>
	{{else}}
	<!-- If no Infoblocks, use Bottom Banners instead -->
	<div class="home-infoblock-layout">
		<!-- Bottom Banners config -->
		{{#each bottomBannerImages}}
		<a href="/search" class="home-infoblock-link">
			<div class="home-infoblock" style="background-image: url({{getThemeAssetsPathWithDefault this}});background-color:darkgray;">
				<div class="home-infoblock-content">
		            <h2 class="home-infoblock-title">{{translate 'Sample Title'}}</h2>
		            <h3 class="home-infoblock-text">{{translate 'Optional text'}}</h3>
		        </div>
			</div>
		</a>
        {{/each}}
    </div>
	{{/if}}

	<div class="home-cms-zone" data-cms-area="home_content_middle" data-cms-area-filters="path"></div>
</div>

<div class="home">
	<!-- FREE TEXT AND IMAGES -->
	<div class="home-page-freetext-wrapper">
		<div class="home-page-freetext">

			<div class="home-page-freetext-content">
		        <div class="home-page-freetext-content-text">
		        	{{#if extraHome.freeTextTitle}}
					<div class="home-page-freetext-content-header">
				        <h3>{{extraHome.freeTextTitle}}</h3>
				    </div>
				    {{/if}}
		        	{{#if extraHome.freeText}}<div class="home-page-freetext-text">{{{extraHome.freeText}}}</div>{{/if}}
		        </div>
				{{#if extraHome.showFreeTextImages}}
		        <div class="home-page-freetext-content-images-wrapper">
					{{#each extraHome.freeTextImages}}
	                <div class="home-page-freetext-content-image"><a href="{{href}}"><img src="{{getThemeAssetsPathWithDefault image}}"></a></div>
					{{/each}}
		        </div>
		        {{/if}}
		    </div>

		</div>
	</div>
</div>
<!-- Standard Carousel -->
<div class="home-nopad">
	<div class="home-slider-container">
			<!--<div>
		{{carouselImages}}
	</div>-->
		<div class="home-image-slider">
			<ul data-slider class="home-image-slider-list">
				{{#each carouselImages}}
					<li>
						<div class="home-slide-main-container" style="background-image: url({{image}});">
							<img src="{{this}}" class="home-slide-image" style="display: none;" />
							<div class="home-slide-caption">
								<h2 class="home-slide-caption-title">{{text}}</h2>
								<p>{{title}}</p>
								<div class="home-slide-caption-button-container">
									<a href="/search" class="home-slide-caption-button">Shop Now <i class="home-slide-button-icon"></i></a>
								</div>
							</div>
						</div>
					</li>
				{{/each}}
			</ul>
		</div>
	</div>
</div>
<!-- Standard Carousel -->
<div class="home-nopad">
    <div class="home-cms-zone" data-cms-area="home_content_bottom" data-cms-area-filters="path"></div>
</div>


{{!----
Use the following context variables when customizing this template:

	imageHomeSize (String)
	imageHomeSizeBottom (String)
	carouselImages (Array)
	bottomBannerImages (Array)

----}}
