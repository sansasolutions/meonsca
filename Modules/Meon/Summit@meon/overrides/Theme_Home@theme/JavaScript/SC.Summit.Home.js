/*
    © 2017 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module Home
define('SC.Summit.Home', [
    'Home.View',
    'underscore',
    'SC.Configuration'
],
function SCHome(
    HomeView,
    _,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            // for Carousel
            var carousel = Configuration.get('home.carousel', []);
            var carouselObj;
            var showCarousel = false;
            // for Infoblocks
            var infoblock = Configuration.get('home.infoblock', []);
            // for Free text and images
            var freeTextImages = Configuration.get('home.freeTextImages', []);

            HomeView.addExtraContextProperty(
                'extraHome',
                'object',
                function HomeViewAddExtraContextProperty(context) {
                    carouselObj = context.carousel;
                    showCarousel = context.showCarousel;

                    if (!_.isEmpty(carouselObj)) {
                        _.each(carouselObj, function mapGroupBanner(val, key) {
                            if (_.isEmpty(val.image)) {
                                val.image = carousel[key].image || '';
                            }
                        });
                    } else {
                        if (typeof showCarousel === 'undefined') {
                            showCarousel = true;
                        }

                        carouselObj = carousel;
                    }

                    return {
                        // @property {Boolean} showCarousel
                        showCarousel: showCarousel && carouselObj && !!carouselObj.length,
                        // @property {Array<Object>} carousel
                        carousel: carouselObj,
                        // @property {Boolean} showInfoblocks
                        showInfoblocks: infoblock && !!infoblock.length,
                        // @property {Array<Object>} infoblock
                        infoblock: infoblock,
                        // @property {String} freeTextTitle - allows markup
                        freeText: _(Configuration.get('home.freeText', '')).translate(),
                        // @property {String} freeTextTitle
                        freeTextTitle: _(Configuration.get('home.freeTextTitle')).translate(),
                        // @property {Boolean} showFreeTextImages
                        showFreeTextImages: freeTextImages && !!freeTextImages.length,
                        // @property {Array<Object>} freeTextImages - the object contains the properties text:String, href:String
                        freeTextImages: freeTextImages
                    };
                }
            );
        }
    };
});
