{{!
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{! Edited for Summit Theme }}

<div class="header-message" data-view="Message.Placeholder"></div>
<!-- phone -->
<div>
	<div class= "et-info">
		<span class= "icon-phone">&nbsp;</span>
		<span class= "et-info-phone"> 0808 118 1922</span>
	</div>
</div>
<!-- end phone -->
<!-- start to nave bar -->
<div class="basket-nav">
        <ul>
            <li>
                <a href="https://knowledge.meonuk.com" target="_blank">KNOWLEDGE BASE</a>
            </li>
						<li>
                <a href="https://news.meonuk.com" target="_blank">NEWS</a>
            </li>
						<li data-view="RequestQuoteWizardHeaderLink"></li>
            <li data-view="QuickOrderHeaderLink"></li>
            <!-- <li>
                <a href="/contact">Contact Us</a>

            <li>
            	<a href="#" data-touchpoint="login" data-hashtag="login-register">Login</a>
            </li>
            <li>
                <a href="#" data-touchpoint="register" data-hashtag="login-register">Register</a>
            </li>
						</li> -->
        </ul>
    </div>
<!-- end top nave bar -->

<div id="header-version-message" class="header-version-message" data-cms-area="header_version_message" data-cms-area-filters="global"></div>

<div class="header-main-wrapper" style="background-image:url('{{backgroundImageUrl}}')">
	<nav class="header-main-nav">
		<div class ="wrapheader">
			<div id="banner-header-top" class="content-banner banner-header-top" data-cms-area="header_banner_top" data-cms-area-filters="global"></div>

			<div class="header-sidebar-toggle-wrapper">
				<button class="header-sidebar-toggle" data-action="header-sidebar-show">
					<i class="header-sidebar-toggle-icon"></i>
				</button>
			</div>

			<div class="header-content">
				<div class="header-logo-wrapper">
					<div data-view="Header.Logo"></div>
				</div>
				<!-- call us
				<div class="cta">
	            <img src="/c.4026778_SB1/sca-dev-kilimanjaro/img/Home/header_cta_telephone.png" width="44">
	            <p>Call us on 0808 118 1922</p>
	            <a href="/lead-form">Offices in the UK &amp; Ireland <span></span></a>
	      </div>
					call us  -->
				<div class="header-right-menu">
					<div class="header-subheader-container">
						<ul class="header-subheader-options">
							{{#if showLanguagesOrCurrencies}}
							<li class="header-subheader-settings">
								<a href="#" class="header-subheader-settings-link" data-toggle="dropdown" title="{{translate 'Settings'}}">
									<i class="header-menu-settings-icon"></i>
									<i class="header-menu-settings-carret"></i>
								</a>
								<div class="header-menu-settings-dropdown">
									<h5 class="header-menu-settings-dropdown-title">{{translate 'Site Settings'}}</h5>
									{{#if showLanguages}}
										<div data-view="Global.HostSelector"></div>
									{{/if}}
									{{#if showCurrencies}}
										<div data-view="Global.CurrencySelector"></div>
									{{/if}}
								</div>
							</li>
							{{/if}}
							<!--<li data-view="StoreLocatorHeaderLink"></li>-->
							<!-- <li data-view="RequestQuoteWizardHeaderLink"></li>-->
							<!--<li data-view="QuickOrderHeaderLink"></li> -->
						</ul>
					</div>
					<div class="header-menu-profile" data-view="Header.Profile"></div>
					<div class="header-menu-locator-mobile" data-view="StoreLocatorHeaderLink"></div>
					<div class="header-menu-searchmobile">
						<button class="header-menu-searchmobile-link" data-action="show-sitesearch" title="{{translate 'Search'}}">
							<i class="header-menu-searchmobile-icon"></i>
						</button>
					</div>

					<div class="header-menu-cart">
						<div class="header-menu-cart-dropdown" >
							<div data-view="Header.MiniCart"></div>
						</div>
					</div>
				</div>
			</div>

			<div id="banner-header-bottom" class="content-banner banner-header-bottom" data-cms-area="header_banner_bottom" data-cms-area-filters="global"></div>
		</div>
	</nav>
</div>

<div class="header-sidebar-overlay" data-action="header-sidebar-hide"></div>
<div class="header-secondary-wrapper" data-view="Header.Menu" data-phone-template="header_sidebar" data-tablet-template="header_sidebar">
</div>

<div class="header-site-search" data-view="SiteSearch" data-type="SiteSearch"></div>



{{!----
Use the following context variables when customizing this template:

	profileModel (Object)
	profileModel.addresses (Array)
	profileModel.addresses.0 (Array)
	profileModel.creditcards (Array)
	profileModel.firstname (String)
	profileModel.paymentterms (undefined)
	profileModel.phoneinfo (undefined)
	profileModel.middlename (String)
	profileModel.vatregistration (undefined)
	profileModel.creditholdoverride (undefined)
	profileModel.lastname (String)
	profileModel.internalid (String)
	profileModel.addressbook (undefined)
	profileModel.campaignsubscriptions (Array)
	profileModel.isperson (undefined)
	profileModel.balance (undefined)
	profileModel.companyname (undefined)
	profileModel.name (undefined)
	profileModel.emailsubscribe (String)
	profileModel.creditlimit (undefined)
	profileModel.email (String)
	profileModel.isLoggedIn (String)
	profileModel.isRecognized (String)
	profileModel.isGuest (String)
	profileModel.priceLevel (String)
	profileModel.subsidiary (String)
	profileModel.language (String)
	profileModel.currency (Object)
	profileModel.currency.internalid (String)
	profileModel.currency.symbol (String)
	profileModel.currency.currencyname (String)
	profileModel.currency.code (String)
	profileModel.currency.precision (Number)
	showLanguages (Boolean)
	showCurrencies (Boolean)
	showLanguagesOrCurrencies (Boolean)
	showLanguagesAndCurrencies (Boolean)
	isHomeTouchpoint (Boolean)
	cartTouchPoint (String)

----}}
