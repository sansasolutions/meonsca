/*
    © 2017 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

// @module Footer
define('SC.Summit.Footer.Simplified', [
    'Footer.Simplified.View',
    'SC.Configuration'
],
function SCSimplifiedFooter(
    FooterSimplifiedView,
    Configuration
) {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            // for Copyright message
            var initialConfigYear = Configuration.get('footer.copyright.initialYear');
            var initialYear = initialConfigYear ? parseInt(initialConfigYear, 10) : new Date().getFullYear();
            var currentYear = new Date().getFullYear();

            FooterSimplifiedView.addExtraContextProperty(
                'extraFooterSimplified',
                'object',
                function FooterSimplifiedViewAddExtraContextProperty() {
                    return {
                        copyright: {
                            hide: !!Configuration.get('footer.copyright.hide'),
                            companyName: Configuration.get('footer.copyright.companyName'),
                            initialYear: initialYear,
                            currentYear: currentYear,
                            showRange: initialYear < currentYear
                        }
                    };
                }
            );
        }
    };
});
