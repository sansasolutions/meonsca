define('SC.Summit.ItemRelations.SC.Configuration', [
    'SC.Configuration'
], function SCItemRelations() {
    'use strict';

    return {
        mountToApp: function mountToApp() {
            if (SC.CONFIGURATION.bxSliderDefaults && SC.CONFIGURATION.bxSliderDefaults.slideWidth) {
                SC.CONFIGURATION.bxSliderDefaults.slideWidth = 272;
                SC.CONFIGURATION.bxSliderDefaults.maxSlides = 4;
            }
        }
    };
});
